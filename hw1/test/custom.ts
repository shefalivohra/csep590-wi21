import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";

describe("custom", () => {
    it("example from assignment", () =>{
        function f() {
            return new EngExp().beginCapture().then(
                new EngExp().endCapture()).asRegExp();
        }

        expect(f).to.throw(EngExpError);
    });

    it("should correctly resolve nested levels calls", () =>{
        const e = new EngExp()
            .startOfLine()
            .beginLevel()
            .match("http")
            .maybe("s")
            .beginLevel()
            .match("://")
            .endLevel()
            .endLevel()
            .beginLevel()
            .maybe("www.")
            .beginLevel()
            .match("shefalivohra.com")
            .endLevel()
            .endLevel()
            .beginLevel()
            .maybe(new EngExp().match("/").beginCapture().anythingBut(" ").endCapture())
            .endLevel()
            .endOfLine()
            .asRegExp();
        expect(e.test("https://www.shefalivohra.com")).to.be.true;
        expect(e.test("http://www.shefalivohra.com")).to.be.true;
        expect(e.test("https://shefalivohra.com")).to.be.true;
        expect(e.test("http://shefalivohra.com")).to.be.true;
        expect(e.test("httpswww.shefalivohra.com")).to.be.false;
        expect(e.test("https://www.google.com")).to.be.false;
        expect(e.test("https://www.shefalivohra.com/")).to.be.true;
        expect(e.test("https://www.shefalivohra.com/foo")).to.be.true;
        expect(e.test("https://www.shefalivohra.com/foo bar")).to.be.false;


        const result = e.exec("https://www.shefalivohra.com/foo");
        expect(result[1]).to.be.equal("foo");
    });

    it("endcapture closes level instead of endlevel - throws", () =>{
        function f() {
            return new EngExp()
                .startOfLine()
                .beginLevel()
                .match("spongebob")
                .maybe(" ")
                .beginLevel()
                .match("squarepants")
                .endLevel()
                .endCapture()
                .endOfLine()
                .asRegExp();
        }
        expect(f).to.throw(EngExpError);
    });

    it("endlevel closes level instead of endcapture - throws", () =>{
        function f() {
            return new EngExp()
                .startOfLine()
                .beginCapture()
                .anythingBut(" ")
                .endLevel()
                .endOfLine()
                .asRegExp();
        }
        expect(f).to.throw(EngExpError);
    });

    it("capture with nested levels", () =>{
        const e = new EngExp()
            .startOfLine()
            .beginLevel()
            .match("http")
            .maybe("s")
            .beginLevel()
            .match("://")
            .endLevel()
            .endLevel()
            .beginLevel()
            .maybe("www.")
            .beginLevel()
            .match("shefalivohra.com")
            .endLevel()
            .endLevel()
            .maybe("/")
            .beginCapture()
                .beginLevel()
                .anythingBut(" ")
                .endLevel()
                .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        expect(e.test("https://www.shefalivohra.com")).to.be.true;
        expect(e.test("http://www.shefalivohra.com")).to.be.true;
        expect(e.test("https://shefalivohra.com")).to.be.true;
        expect(e.test("http://shefalivohra.com")).to.be.true;
        expect(e.test("httpswww.shefalivohra.com")).to.be.false;
        expect(e.test("https://www.google.com")).to.be.false;
        expect(e.test("https://www.shefalivohra.com/")).to.be.true;
        expect(e.test("https://www.shefalivohra.com/foo")).to.be.true;
        expect(e.test("https://www.shefalivohra.com/foo bar")).to.be.false;
        expect(e.test("https://www.shefalivohra.com/foo/bar")).to.be.true;

        const result = e.exec("https://www.shefalivohra.com/foo/bar");
        expect(result[1]).to.be.equal("foo/bar");
    });

    it("then vs. level - same output", () => {
        const e1 = new EngExp()
            .startOfLine()
            .beginLevel()
            .match("http")
            .maybe("s")
            .beginLevel()
            .match("://")
            .endLevel()
            .endLevel()
            .beginLevel()
            .maybe("www.")
            .beginLevel()
            .match("shefalivohra.com")
            .endLevel()
            .endLevel()
            .maybe("/")
            .beginCapture()
                .beginLevel()
                .anythingBut(" ")
                .endLevel()
                .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        const e2 = new EngExp()
            .startOfLine()
            .then(new EngExp()
                .match("http")
                .maybe("s")
                .then(new EngExp().match("://")))
            .then(new EngExp()
                .maybe("www.")
                .then(new EngExp().match("shefalivohra.com")))
            .maybe("/")
            .beginCapture()
                .then(new EngExp().anythingBut(" "))
                .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        
        expect(e1.toString()).equals(e2.toString());
    })
});
