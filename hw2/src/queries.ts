import * as q from "./q";
const Q = q.Q;

//// 1.2 write a query

export const theftsQuery = new q.ThenNode(
    new q.IdNode(),
    new q.FilterNode((x) => x[2].includes('THEFT'))
);

export const autoTheftsQuery = new q.ThenNode(
    new q.IdNode(),
    new q.FilterNode((x) => x[3] === 'MOTOR VEHICLE THEFT')
);

//// 1.4 clean the data

export const cleanupQuery = new q.ThenNode(
    new q.IdNode(),
    new q.ApplyNode((x) => {
        return {
            description: x[2],
            category: x[3],
            area: x[4],
            date: x[5]
        }
    })
);

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = Q.apply((x) => {
    return {
        description: x[2],
        category: x[3],
        area: x[4],
        date: x[5]
    }
});
export const theftsQuery2 = Q.filter((x) => x.description.includes('THEFT'));
export const autoTheftsQuery2 = Q.filter((x) => x.category === 'MOTOR VEHICLE THEFT');

//// 4 put your queries here (remember to export them for use in tests)

export const hillQuery = Q.filter((x) => x.area.includes('HILL'));
export const parkQuery = Q.filter((x) => x.area.includes('PARK'));
export const cartesianQuery = hillQuery.product(parkQuery);
export const joinQuery = hillQuery.join(parkQuery, "category");
export const joinQuery2 = hillQuery.join(parkQuery, (h, p) => !h.description.includes('THEFT') && !p.description.includes('THEFT'));
export const joinQuery3 = hillQuery.join(parkQuery, (h, p) => h.description.includes('ASSAULT') && p.description.includes('ASSAULT'));
