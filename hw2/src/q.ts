/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

// This class represents all AST Nodes.
export class ASTNode {
    public readonly type: string;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Execute not implemented for " + this.type + " node.");
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 implement call-chaining

    filter(predicate: (datum: any) => boolean, relation?: string): ASTNode {
        return new ThenNode(
            this,
            new FilterNode(predicate, relation)
        );
    }

    apply(callback: (datum: any) => any, sortFn?: (datum1: any, datum2: any) => number): ASTNode {
        return new ThenNode(
            this,
            new ApplyNode(callback, sortFn)
        );
    }

    count(): ASTNode {
        return new ThenNode(
            this,
            new CountNode()
        );
    }

    product(query: ASTNode): ASTNode {
        return new CartesianProductNode(
            this,
            query
        );
    }

    join(query: ASTNode, relation: string | ((left: any, right: any) => any)): ASTNode {
        if (typeof (relation) === "string") {
            return this.product(query).filter(x => x.left[relation] == x.right[relation], relation).apply(x => x.right);
        }
        else {
            return this.product(query).filter(x => relation(x.left, x.right)).apply(x => x.right);
        }
    }

    sort(c: (datum1: any, datum2: any) => number): ASTNode {
        const unsorted = [];
        return this.apply(x => {
            unsorted.push(x);
            return unsorted.length - 1;
        }, c).apply(i => unsorted.sort(c)[i]);
    }
}

// The Id node just outputs all records from input.
export class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return data;
    }
}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;
    relation?: string;

    constructor(predicate: (datum: any) => boolean, relation?: string) {
        super("Filter");
        this.predicate = predicate;
        this.relation = relation;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return data.filter(this.predicate);
    }
}

// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return this.second.execute(this.first.execute(data));
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
    callback: (datum: any) => any;
    sortFn?: (datum1: any, datum2: any) => number

    constructor(callback: (datum: any) => any, sortFn?: (datum1: any, datum2: any) => number) {
        super("Apply");
        this.callback = callback;
        this.sortFn = sortFn;
    }

    execute(data: any[]): any {
        return data.map(this.callback);
    }
}

export class CountNode extends ASTNode {
    constructor() {
        super("Count");
    }

    execute(data: any[]): any {
        return [data.length];
    }
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    const oldOptimize = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function (this: ASTNode): ASTNode {
        const newThis = oldOptimize.call(this);
        return opt.call(newThis) || newThis;
    };
}

// For example, the following small optimization removes unnecessary Id nodes.
AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    if (this.first instanceof IdNode) {
        return this.second;
    } else if (this.second instanceof IdNode) {
        return this.first;
    } else {
        return null;
    }
});

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    if (this.first instanceof FilterNode && this.second instanceof FilterNode) {
        return new FilterNode(this.first.predicate && this.second.predicate);
    } else if (this.first instanceof ThenNode && this.first.second instanceof FilterNode && this.second instanceof FilterNode) {
        return new FilterNode(this.first.second.predicate && this.second.predicate);
    }

    return this;
});

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    if (this.first instanceof FilterNode && this.second instanceof CountNode) {
        return new CountIfNode(this.first.predicate);
    } else if (this.first instanceof ThenNode && this.first.second instanceof FilterNode && this.second instanceof CountNode) {
        return new CountIfNode(this.first.second.predicate);
    }

    return this;
});

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    //             then
    //             /   \
    //         then      apply
    //        /     \
    //    product    filter
    if (this.second instanceof ApplyNode && this.first instanceof ThenNode && this.first.second instanceof FilterNode && this.first.first instanceof CartesianProductNode) {
        if (this.first.second.relation) {
            return new HashJoinNode(this.first.second.relation, this.first.first.left, this.first.first.right);
        }
        return new JoinNode(this.first.second.predicate, this.first.first.left, this.first.first.right);
    }

    return this;
});

AddOptimization(ThenNode, function (this: ThenNode): ASTNode {
    if (this instanceof ThenNode && this.first instanceof ApplyNode && this.second instanceof ApplyNode && this.first.sortFn != undefined) {
        return new SortByNode(this, this.first.sortFn);
    }

    return this;
});



//// 2.2 internal node types and CountIf

export class CountIfNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("CountIf");
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        return [data.filter(this.predicate).length];
    }
}

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;

    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        this.left = left;
        this.right = right;
    }

    execute(data: any[]): any {
        const outLeft = this.left.execute(data);
        const outRight = this.right.execute(data);
        const output = []
        outLeft.forEach(l => {
            outRight.forEach(r => {
                output.push({ left: l, right: r });
            });
        });
        return output;
    }
}

//// 3.2-3.6 joins and hash joins

/* Hint: Recall from the spec that a join of two arrays P and Q by a
   function f(l, r) is the array with one entry for each pair of a record
   in P and Q for which f returns true and that each entry should have all
   fields and values that either record in the pair had.

   Most notably, if both records had the same field, use the value from the
   record from Q. In JavaScript, this can be achieved with Object.assign:
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

export class JoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    predicate: (datum: any) => boolean;


    constructor(predicate: (datum: any) => boolean, left: ASTNode, right: ASTNode) { // you may want to add some proper arguments to this constructor
        super("Join");
        this.left = left;
        this.right = right;
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        const outLeft = this.left.execute(data);
        const outRight = this.right.execute(data);
        const output = []
        outLeft.forEach(l => {
            outRight.forEach(r => {
                if (this.predicate({ left: l, right: r })) {
                    output.push(r)
                }
            });
        });
        return output;
    }
}

/* Hint: While optimizing the fluent join to use your internal JoinNode,
   if you find yourself needing to compare two functions for equality,
   you can do so with the Javascript operator `===` (pointer equality).
 */

export class HashJoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    relation: string;

    constructor(relation: string, left: ASTNode, right: ASTNode) { // you may want to add some proper arguments to this constructor
        super("HashJoin");
        this.left = left;
        this.right = right;
        this.relation = relation;
    }

    execute(data: any[]): any {
        const outLeft = this.left.execute(data);
        const outRight = this.right.execute(data);
        var leftMap = {}
        var rightMap = {}
        outLeft.forEach(l => {
            if (!(l[this.relation] in leftMap)) {
                leftMap[l[this.relation]] = [];
            }
            leftMap[l[this.relation]].push(l);
        })
        outRight.forEach(r => {
            if (!(r[this.relation] in rightMap)) {
                rightMap[r[this.relation]] = [];
            }
            rightMap[r[this.relation]].push(r);
        })

        var output = []
        for (const key in leftMap) {
            if (key in rightMap) {
                for (const _ in leftMap[key]) {
                    for (const rVal in rightMap[key]) {
                        output.push(rVal)
                    }
                }
            }
        }

        return output
    }
}

export class SortByNode extends ASTNode {
    p: ASTNode;
    c: (datum1: any, datum2: any) => number;

    constructor(p: ASTNode, c: (datum1: any, datum2: any) => number) {
        super("SortBy");
        this.p = p;
        this.c = c;
    }

    execute(data: any[]): any {
        const unsorted = this.p.execute(data);
        return unsorted.sort(this.c);
    }
}