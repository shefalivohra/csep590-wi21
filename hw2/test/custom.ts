import { expect } from "chai";

import * as q from "../src/q";
const Q = q.Q;

import { crimeData } from "./data";
const rawData = crimeData.data;
import * as queries from "../src/queries";

function flattenThenNodes(query) {
    if (query instanceof q.ThenNode) {
        return flattenThenNodes(query.first).concat(flattenThenNodes(query.second));
    } else if (query instanceof q.IdNode) {
        return [];
    } else {
        return [query];
    }
}

function expectQuerySequence(query, seq) {
    const flat = flattenThenNodes(query);
    expect(flat.length).to.be.equal(seq.length);
    for (let i = 0; i < flat.length; i++) {
        expect(flat[i].type).to.be.equal(seq[i]);
    }
}

function cleanData() {
    const out = [];
    for (const x of rawData) {
        out.push({ description: x[2], category: x[3], area: x[4], date: x[5] });
    }
    return out;
}

// You should provide at least 5 tests, using your own queries from Part 4
// and either the crime data from test/data.ts or your own data. You can look
// at test/q.ts to see how to import and use the queries and data.
// The outline of the first test has been provided as an example - you should
// modify it so it actually does something interesting.

describe("Custom: Problems 1-3", () => {

    it("filter node only", () => {
        const query = new q.FilterNode((x) => x[0] % 2 === 1);
        const out = query.execute(rawData);

        const good = [];
        for (const datum of rawData) {
            if ((datum[0] as number) % 2) {
                good.push(datum);
            }
        }
        expect(out).to.be.an("Array");
        expect(out).to.deep.equal(good);
    });

    it("Optimize three filters", () => {
        const query = Q.filter((x) => !!x).filter((x) => !!x).filter((x) => !!x);
        const opt = query.optimize();
        expectQuerySequence(opt, ["Filter"]);
    });

    it("Optimize nested filters", () => {
        const query = new q.ThenNode(
            new q.ThenNode(
                new q.ApplyNode((x) => !!x),
                new q.FilterNode((x) => !!x),
            ),
            new q.FilterNode((x) => !!x));
        const opt = query.optimize();
        expectQuerySequence(opt, ["Filter"]);
    });

    it("Optimize nested Internal node types and CountIf", () => {
        const query = new q.ThenNode(
            new q.ThenNode(
                new q.ApplyNode((x) => !!x),
                new q.FilterNode((x) => !!x)
            ),
            new q.CountNode());
        const opt = query.optimize();
        expectQuerySequence(opt, ["CountIf"]);
    });

});

describe("Custom: Problem 4", () => {
    it("Cleans and Filters hills and parks", () => {
        const data = cleanData();
        const out = queries.cleanupQuery2.execute(rawData);
        expect(out).to.deep.equal(data);

        const good1 = [];
        const good2 = [];
        for (const datum of data) {
            if (datum.area.match(/HILL/)) {
                good1.push(datum);
            }
            if (datum.area.match(/PARK/)) {
                good2.push(datum);
            }
        }
        const out1 = queries.hillQuery.execute(out);
        const out2 = queries.parkQuery.execute(out);

        expect(out1).to.deep.equal(good1);
        expect(out2).to.deep.equal(good2);
    });

    it("Cartesian product of hills and parks", () => {
        const data = cleanData();
        const out = queries.cleanupQuery2.execute(rawData);
        expect(out).to.deep.equal(data);

        let hillCount = 0;
        let parkCount = 0;
        for (const datum of data) {
            if (datum.area.match(/HILL/)) {
                hillCount += 1;
            }
            if (datum.area.match(/PARK/)) {
                parkCount += 1
            }
        }

        const query = queries.cartesianQuery.count();
        const output = query.execute(out);
        expect(output).to.be.an("Array");
        expect(output).to.have.length(1);
        expect(output[0]).to.equal(hillCount * parkCount);
    });

    it("Joins hills and parks on string", () => {

        let good = 0;
        for (const datum1 of rawData) {
            if (String(datum1[4]).match(/HILL/)) {
                for (const datum2 of rawData) {
                    if ((String(datum2[4]).match(/PARK/))) {
                        if (datum1[3] === datum2[3]) {
                            good++;
                        }
                    }
                }
            }
        }

        const query = queries.joinQuery.count();
        const out = queries.cleanupQuery2.execute(rawData);

        const output1 = query.execute(out);
        expect(output1).to.be.an("Array");
        expect(output1).to.have.length(1);
        expect(output1[0]).to.equal(good);


        const output2 = query.optimize().execute(out);
        expect(output2).to.be.an("Array");
        expect(output2).to.have.length(1);
        expect(output2[0]).to.equal(good);
    });

    it("Joins hills and parks on predicate", () => {
        let good = 0;
        for (const datum1 of rawData) {
            if (String(datum1[4]).match(/HILL/)) {
                for (const datum2 of rawData) {
                    if ((String(datum2[4]).match(/PARK/))) {
                        if (!(String(datum1[2])).match(/THEFT/) && !(String(datum2[2]).match(/THEFT/))) {
                            good++;
                        }
                    }
                }
            }
        }

        const query = queries.joinQuery2.count();
        const out = queries.cleanupQuery2.execute(rawData);

        const output1 = query.execute(out);
        expect(output1).to.be.an("Array");
        expect(output1).to.have.length(1);
        expect(output1[0]).to.equal(good);


        const output2 = query.optimize().execute(out);
        expect(output2).to.be.an("Array");
        expect(output2).to.have.length(1);
        expect(output2[0]).to.equal(good);
    });

    it("Joins hills and parks on predicate a different way", () => {
        let good = 0;
        for (const datum1 of rawData) {
            if (String(datum1[4]).match(/HILL/)) {
                for (const datum2 of rawData) {
                    if ((String(datum2[4]).match(/PARK/))) {
                        if ((String(datum1[2])).match(/ASSAULT/) && (String(datum2[2]).match(/ASSAULT/))) {
                            good++;
                        }
                    }
                }
            }
        }

        const query = queries.joinQuery3.count();
        const out = queries.cleanupQuery2.execute(rawData);

        const output1 = query.execute(out);
        expect(output1).to.be.an("Array");
        expect(output1).to.have.length(1);
        expect(output1[0]).to.equal(good);


        const output2 = query.optimize().execute(out);
        expect(output2).to.be.an("Array");
        expect(output2).to.have.length(1);
        expect(output2[0]).to.equal(good);
    });
});

describe("EC: Sort", () => {
    it("sorting with chaining", () => {
        const data = cleanData();
        const out = queries.cleanupQuery2.execute(rawData);
        expect(out).to.deep.equal(data);

        const good1 = [];
        const good2 = [];
        for (const datum of data) {
            if (datum.area.match(/HILL/)) {
                good1.push(datum);
            }
            if (datum.area.match(/PARK/)) {
                good2.push(datum);
            }
        }
        const sortFn = (data1, data2) => {
            if (data1.category > data2.category) return 1
            else if (data1.category < data2.category) return -1
            return 0
        }
        const want1 = good1.sort(sortFn);
        const want2 = good2.sort(sortFn);

        const got1 = queries.hillQuery.sort(sortFn).execute(out);
        expect(want1).to.deep.equal(got1);
        const got2 = queries.parkQuery.sort(sortFn).execute(out);
        expect(want2).to.deep.equal(got2);
    })

    it("sorting with SortByNode", () => {
        const sortFn = (data1, data2) => {
            if (data1.category > data2.category) return 1
            else if (data1.category < data2.category) return -1
            return 0
        }
        const query = new q.SortByNode(queries.cleanupQuery2, sortFn);
        const out = query.execute(rawData);

        const good = cleanData().sort(sortFn);

        expect(out).to.deep.equal(good);
    })

    it("Optimize sort to sort by node", () => {
        const query = Q.sort((a, b) => { return 0 });
        const opt = query.optimize();
        expectQuerySequence(opt, ["SortBy"]);
    });
});
