import {expect} from "chai";

import * as q from "../src/q";
const Q = q.Q;
import * as queries from "../src/queries";

import {crimeData} from "./data";
const rawData = crimeData.data;

function flattenThenNodes(query) {
    if (query instanceof q.ThenNode) {
        return flattenThenNodes(query.first).concat(flattenThenNodes(query.second));
    } else if (query instanceof q.IdNode) {
        return [];
    } else {
        return [query];
    }
}

function expectQuerySequence(query, seq) {
    const flat = flattenThenNodes(query);
    expect(flat.length).to.be.equal(seq.length);
    for (let i = 0; i < flat.length; i++) {
        expect(flat[i].type).to.be.equal(seq[i]);
    }
}

function cleanData() {
    const out = [];
    for (const x of rawData) {
        out.push({description: x[2], category: x[3], area: x[4], date: x[5]});
    }
    return out;
}


describe("Problem 1", () => {
    it("Executing queries", () => {
        const query = new q.ThenNode(
            new q.IdNode(),
            new q.FilterNode((x) => x[0] % 2 === 1));
        const out = query.execute(rawData);

        const good = [];
        for (const datum of rawData) {
            if ((datum[0] as number) % 2) {
                good.push(datum);
            }
        }
        expect(out).to.be.an("Array");
        expect(out).to.deep.equal(good);
    });

    it("Write a query", () => {
        const good1 = [];
        const good2 = [];
        for (const datum of rawData) {
            if ((datum[2] as string).match(/THEFT/)) {
                good1.push(datum);
            }
            if ((datum[3] as string) === "MOTOR VEHICLE THEFT") {
                good2.push(datum);
            }
        }

        const out1 = queries.theftsQuery.execute(rawData);
        const out2 = queries.autoTheftsQuery.execute(rawData);

        expect(out1).to.deep.equal(good1);
        expect(out2).to.deep.equal(good2);
    });

    it("Add Apply and Count nodes", () => {
        function even(x) {
            return (x % 2) === 0;
        }

        const query = new q.ThenNode(
            new q.ApplyNode((x) => x[0]),
            new q.ThenNode(
                new q.FilterNode(even),
                new q.CountNode()));
        const out = query.execute(rawData);

        let good = 0;
        for (const datum of rawData) {
            if (even(datum[0])) {
                good++;
            }
        }
        expect(out).to.be.an("Array");
        expect(out).to.have.length(1);
        expect(out[0]).to.equal(good);
    });

    it("Clean the data", () => {
        const out = queries.cleanupQuery.execute(rawData);
        expect(out).to.deep.equal(cleanData());
    });

    it("Implement a call-chaining interface", () => {
        const query = Q.apply((x) => x[0] % 2)
            .filter((x) => !!x)
            .count();
        const out = query.execute(rawData);

        let good = 0;
        for (const datum of rawData) {
            if ((datum[0] as number) % 2) {
                good++;
            }
        }

        expect(out).to.be.an("Array");
        expect(out).to.have.length(1);
        expect(out[0]).to.equal(good);
    });

    it("Reimplement queries with call-chaining", () => {
        const data = cleanData();
        const out = queries.cleanupQuery2.execute(rawData);
        expect(out).to.deep.equal(data);

        const good1 = [];
        const good2 = [];
        for (const datum of data) {
            if (datum.description.match(/THEFT/)) {
                good1.push(datum);
            }
            if (datum.category === "MOTOR VEHICLE THEFT") {
                good2.push(datum);
            }
        }

        const out1 = queries.theftsQuery2.execute(data);
        const out2 = queries.autoTheftsQuery2.execute(data);
        expect(out1).to.deep.equal(good1);
        expect(out2).to.deep.equal(good2);
    });
});


describe("Problem 2", () => {
    it("Optimize filters", () => {
        const query = Q.filter((x) => !!x).filter((x) => !!x);
        const opt = query.optimize();
        expectQuerySequence(opt, ["Filter"]);
    });

    it("Internal node types and CountIf", () => {
        const query = Q.filter((x) => !!(x[0] % 2)).count();
        const opt = query.optimize();
        expectQuerySequence(opt, ["CountIf"]);
    });

    it("Multiple filters internal node types and CountIf", () => {
        const query = Q.filter((x) => !!(x[0] % 2)).filter((x) => !!(x[0] % 2)).filter((x) => !!(x[0] % 2)).filter((x) => !!(x[0] % 2)).filter((x) => !!(x[0] % 2)).count();
        const opt = query.optimize();
        expectQuerySequence(opt, ["CountIf"]);
    });
});


describe("Problem 3", () => {
    it("Cartesian products", () => {
        const testData = ["a", "b", "c"];
        const testExpected = [
            {left: "a", right: "a"},
            {left: "a", right: "b"},
            {left: "a", right: "c"},
            {left: "b", right: "a"},
            {left: "b", right: "b"},
            {left: "b", right: "c"},
            {left: "c", right: "a"},
            {left: "c", right: "b"},
            {left: "c", right: "c"},
        ];

        const query = new q.CartesianProductNode(new q.IdNode(), new q.IdNode());
        const out = query.execute(testData);
        const len = testData.length;
        expect(out).to.have.length(len * len);
        expect(out).to.deep.equal(testExpected);
    });

    it("Joins", () => {

        // Assign increasing IDs for this test.
        var rawData2 = [];
        var i = 0;
        for (let datum of rawData) {
            var newDatum = datum;
            newDatum[0] = i;
            rawData2.push(newDatum);
            i += 1;
        }

        const query = Q.join(Q, (l, r) => l[0] === r[0] - 1).apply((x) => x[0]);
        const out = query.execute(rawData2);
        const expected = rawData2.slice(1).map((x) => x[0]);
        expect(out).to.deep.equal(expected);
    });

    it("Joins Objects", () => {
        const query = Q.join(Q, (l, r) => true);
        const data = [{a: 0, b: 2}];
        const expected = [{a: 0, b: 2}];

        expect(query.execute(data)).to.deep.equal(expected);
        expect(query.optimize().execute(data)).to.deep.equal(expected);
    });

    it("Optimizing joins", () => {
        const query = Q.join(Q, (l, r) => l[0] === r[0] - 1);

        const opt = query.optimize();
        expectQuerySequence(opt, ["Join"]);

        const out = opt.execute(rawData);
        expect(out).to.deep.equal(query.execute(rawData));
    });

    it("Joins on fields", () => {
        const query = Q.join(Q, "category").count();
        const out = query.execute(cleanData());

        let good = 0;
        for (const datum1 of rawData) {
            for (const datum2 of rawData) {
                if (datum1[3] === datum2[3]) {
                    good++;
                }
            }
        }

        expect(out).to.be.an("Array");
        expect(out).to.have.length(1);
        expect(out[0]).to.equal(good);
    });

    it("Implement hash joins", () => {
        const query = new q.HashJoinNode("category", new q.IdNode(), new q.IdNode()).count();
        const out = query.execute(cleanData());

        let good = 0;
        for (const datum1 of rawData) {
            for (const datum2 of rawData) {
                if (datum1[3] === datum2[3]) {
                    good++;
                }
            }
        }

        expect(out).to.be.an("Array");
        expect(out).to.have.length(1);
        expect(out[0]).to.equal(good);
    });

    it("Allows duplicate records in HashJoin", () => {
        const query = Q.join(Q, "a");
        const datum = {a: 3, b: 2};
        const data = [datum, datum];

        expect(query.execute(data)).to.have.length(4);
        expect(query.optimize().execute(data)).to.have.length(4);
    });

    it("Optimize joins on fields to hash joins", () => {
        const query = Q.join(Q, "type");
        const opt = query.optimize();
        expectQuerySequence(opt, ["HashJoin"]);
    });
});
