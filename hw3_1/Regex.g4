grammar Regex;

re
    : expression EOF
    | alt EOF
    ;

alt
    : (expression SEPARATOR)* expression
    ;

expression
    : (elems+=element)*
    ;

element
    : a=atom            # AtomicElement
    | a=atom m=MODIFIER # ModifiedElement
    ;

atom
    : c=CHARACTER # CharAtom
    | OPENPAREN alt CLOSEPAREN # ParenAtom
    ;

CHARACTER : [a-zA-Z0-9.] ;
OPENPAREN : [(] ;
CLOSEPAREN : [)] ;
SEPARATOR : [|] ;
MODIFIER : [?*] ;
WS : [ \t\r\n]+ -> skip ;
