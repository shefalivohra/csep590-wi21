class Stream {
    constructor() {
        this.subscribedFunctions = [];
    }

    // Add Stream methods here.

    subscribe(func) {
        this.subscribedFunctions.push(func);
    }

    _push(item) {
        this.subscribedFunctions.forEach(func => func(item));
    }

    _push_many(items) {
        items.forEach(item => {
            this._push(item);
        })
    }

    first() {
        const newStream = new Stream();
        var push = true
        this.subscribe(x => {
            if (push) {
                newStream._push(x);
                push = false;
            }
        });
        return newStream;
    }

    map(f) {
        const newStream = new Stream();
        this.subscribe(x => newStream._push(f(x)));
        return newStream;
    }

    filter(f) {
        const newStream = new Stream();
        this.subscribe(x => { if (f(x)) { newStream._push(x) } });
        return newStream;
    }

    distinctUntilChanged() {
        const newStream = new Stream();
        var lastSeen = null;
        this.subscribe(x => {
            if (lastSeen == null || lastSeen != x) {
                newStream._push(x);
                lastSeen = x;
            }
        });
        return newStream;
    }

    flatten() {
        const newStream = new Stream();
        this.subscribe(x => {
            if (Array.isArray(x)) {
                newStream._push_many(x);
            } else {
                newStream._push(x);
            }
        });
        return newStream;
    }

    scan(f, acc) {
        const newStream = new Stream();
        var newAcc = acc;
        this.subscribe(x => {
            var item = f(newAcc, x)
            newStream._push(item);
            newAcc = item;
        });
        return newStream;
    }

    join(B) {
        const newStream = new Stream();
        this.subscribe(x => newStream._push(x));
        B.subscribe(x => newStream._push(x));
        return newStream;
    }

    combine() {
        const newStream = new Stream();
        this.subscribe(x => x.subscribe(y => newStream._push(y)));
        return newStream;
    }

    zip(B, f) {
        const newStream = new Stream();
        const helperStream = new Stream();
        var lastA = null;
        var lastB = null;
        this.subscribe(x => {
            lastA = x;
            helperStream._push(x);
        });
        B.subscribe(x => {
            lastB = x;
            helperStream._push(x);
        });
        helperStream.subscribe(_ => {
            if (lastA != null && lastB != null) {
                newStream._push(f(lastA, lastB));
            }
        })
        return newStream;
    }

    throttle(N) {
        const newStream = new Stream();
        var throttleBacklog = [];
        var lastUpdateTime = null;

        this.subscribe(x => {
            throttleBacklog.push(x);
            setTimeout(() => {
                if (throttleBacklog.length > 0) {
                    var currentDate = new Date();
                    if (lastUpdateTime == null || Math.abs(currentDate - lastUpdateTime) > N) {
                        newStream._push(x)
                        throttleBacklog = [];
                        lastUpdateTime = currentDate;
                    }
                }
            }, N);
        });
        return newStream;
    }

    unique(f) {
        const newStream = new Stream();
        var seenValues = new Set();
        this.subscribe(x => {
            if (!seenValues.has(f(x))) {
                seenValues.add(f(x));
                newStream._push(x);
            }
        });
        return newStream;
    }

    latest() {
        const newStream = new Stream();
        const helperStream = new Stream();
        var currStream = -1;
        var streamCounter = 0;

        this.subscribe(s => {
            s.subscribe(x => {
                const streamNum = streamCounter;
                helperStream._push([x, streamNum]);
            });
            streamCounter += 1;
        });

        helperStream.subscribe(info => {
            const x = info[0];
            const streamSource = info[1];
            if (streamSource >= currStream) {
                newStream._push(x);
                currStream = streamSource;
            }
        });

        return newStream;
    }

    static ajax(_url, _data) {
        var out = new Stream();
        $.ajax({
            url: _url,
            jsonp: "callback",
            dataType: "jsonp",
            data: _data,
            success: function (data) { out._push(data[1]); }
        });
        return out;
    }

    static timer(N) {
        var out = new Stream();
        setInterval(() => out._push(new Date()), N);
        return out;
    }

    static dom(element, eventname) {
        var out = new Stream();
        element.on(eventname, (e) => out._push(e));
        return out;
    }

    static url(url) {
        var out = new Stream();
        $.get(url, (response) => out._push(response), "json");
        return out
    }
}

if (typeof exports !== "undefined") {
    exports.Stream = Stream;
}

// dependency injection let's do it
function setup($) {
    const FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

    function WIKIPEDIAGET(s) {
        return Stream.ajax("https://en.wikipedia.org/w/api.php", { action: "opensearch", search: s, namespace: 0, limit: 10 });
    }

    // Add your hooks to implement Parts 2-4 here.

    // Timer
    timerStream = Stream.timer(2000);
    timerStream.subscribe(x => $("#time").text(x));

    // Button Click
    clickStream = Stream.dom($("#button"), 'click');
    clickCount = 0;
    clickStream.subscribe(_ => {
        clickCount += 1;
        $("#clicks").text(clickCount);
    });

    // Mouse Position
    mouseStream = Stream.dom($("#mousemove"), 'mousemove');
    positionStream = mouseStream.throttle(2000);
    positionStream.subscribe(e => {
        $("#mouseposition").text("(" + e.offsetX + "," + e.offsetY + ")");
    });


    // Fire logs!
    fireSearchText = '';
    fireInputStream = Stream.dom($("#firesearch"), 'input').map(e => {
        fireSearchText = e.target.value;
        return fireSearchText;
    });

    fireTimeStream = Stream.timer(60000).map(d => fireSearchText);
    fireLogsStream = fireInputStream.join(fireTimeStream);

    fireLogsStream.subscribe(searchText => {
        var numFireLogs = 0;
        Stream.url(FIRE911URL)
        .map(arr => arr.sort(function (a, b) {
            if (a['updated_at'] < b['updated_at']) {
                return -1;
            } else if (a['updated_at'] > b['updated_at']) {
                return 1;
            }
            return 0;
        }))
        .map(x => {
            $("#fireevents").empty();
            return x;
        })
        .flatten()
        .unique(x=> x['id'])
        .map(log => log['405818852'])
        .filter(log => log.toUpperCase().includes(searchText.toUpperCase()))
        .subscribe(fireLog => {
            if (numFireLogs < 10) {
                $("#fireevents").append($("<li></li>").text(fireLog));
                numFireLogs += 1;
            }
        });
    });

    // Wikipedia Search
    wikiInputStream = Stream.dom($("#wikipediasearch"), 'input')
    .map(e => WIKIPEDIAGET(e.target.value))
    .latest()
    .subscribe(results => {
        $("#wikipediasuggestions").empty();
        results.forEach(result => {
            $("#wikipediasuggestions").append($("<li></li>").text(result));
        });
    });

    // Loading Bar
    var totalNumRequests = 10;
    var numRequestsSeen = 0;
    var allRequestsSeen = false;
    loadMoreStream = Stream.dom($("#load-more"), 'click').map(e => 'more');
    loadCompleteStream = Stream.dom($("#load-complete"), 'click').map(e => 'complete');
    loadStream = loadMoreStream.join(loadCompleteStream);

    loadStream.subscribe(loadType => {
        if (!allRequestsSeen) {
            if (loadType == 'complete') {
                // make the progress container full green
                $("#progress").addClass("progress finished");
                allRequestsSeen = true;
            } else {
                numRequestsSeen += 1;
                if (numRequestsSeen == totalNumRequests) {
                    totalNumRequests *= 2;
                }
                // update progress bar
                $("#progress").css('width', `${numRequestsSeen*100/totalNumRequests}%`);
            }
        }
    })
}
