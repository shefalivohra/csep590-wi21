parser grammar HandlebarsParser;

options { tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

blockElement
    : o=blockOpen element* c=blockClose
    ;

blockOpen : START BLOCK id=ID (a+=expressionInternal)* END ;

blockClose : START CLOSE_BLOCK id=ID END ;

expressionElement
    : START e=expressionInternal END
    ;

expressionInternal
    : l=literal | d=dataLookup | h=helperApp
    | OPEN_PAREN e=expressionInternal CLOSE_PAREN
    ; 
    
literal
    : FLOAT | INTEGER | STRING ;
    
dataLookup
    : ID ;

helperApp
    : i=ID (a+=expressionInternal)+
    ;

rawElement
    : TEXT
    | BRACE TEXT
    ;

commentElement : START COMMENT END_COMMENT ;
