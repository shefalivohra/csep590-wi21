function startsWith(ctx, field, letter) {
    return field.startsWith(letter);
 }

 const ctx = {people:[
   {names: {human: "Allison", dogs: {names:[{name:"Albert"}, {name:"Al"}, {name:"Bud"}]}}},
   {names: {human: "AJ", dogs: {names:[{name:"AJJ"}]}}},
   {names: {human: "Shefali", dogs: {names:[{name:"Albert"}, {name:"Al"}, {name:"Bud"}]}}},
   {names: {human: "Alex", dogs: {names:[{name:"Jack"}, {name:"Jill"}]}}}
   ]};
 
 exports.helpers = [
    ["startsWith", startsWith]
];
 exports.blockHelpers = [
 ];
 exports.ctx = ctx;
 exports.description = "nested 'with' blocks";
 