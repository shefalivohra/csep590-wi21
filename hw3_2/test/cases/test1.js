function isEven(ctx, num) {
    return num % 2 == 0;
 }

 const ctx = {integers:[
    {num: 1},
    {num: 2},
    {num: 23},
    {num: -4},
    {num: -2},
    {num: -1},
    {num: 3591},
    ]}
 
 exports.helpers = [
    ["isEven", isEven]
];
 exports.blockHelpers = [
 ];
 exports.ctx = ctx;
 exports.description = "'if' block with context refs inside block";
 