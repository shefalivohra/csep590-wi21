const antlr4 = require("antlr4");
const HandlebarsLexer = require("../gen/HandlebarsLexer").HandlebarsLexer;
const HandlebarsParser = require("../gen/HandlebarsParser").HandlebarsParser;
const HandlebarsParserListener = require("../gen/HandlebarsParserListener").HandlebarsParserListener;

function escapeString(s) {
    return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
        switch (c) {
            case '"':
            case "'":
            case "\\":
                return "\\" + c;
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\u2028":
                return "\\u2028";
            case "\u2029":
                return "\\u2029";
        }
    });
}

class HandlebarsCompiler extends HandlebarsParserListener {
    constructor() {
        super();
        this._inputVar = "_$ctx";
        this._outputVar = "_$result";
        this._helpers = {expr: {}, block: {}};
        this._usedHelpers = new Set();
        this._usedBlocks = new Set();

        // Standard Helpers
        this.registerBlockHelper('if', if_func);
        this.registerBlockHelper('each', each_func);
        this.registerBlockHelper('with', with_func);
    }

    registerExprHelper(name, helper) {
        this._helpers.expr[name] = helper;
    }

    registerBlockHelper(name, helper) {
        this._helpers.block[name] = helper;
    }

    compile(template) {
        this._usedHelpers = new Set();
        this._usedBlocks = new Set();
        this._bodyStack = [];
        this.pushScope();

        const chars = new antlr4.InputStream(template);
        const lexer = new HandlebarsLexer(chars);
        const tokens = new antlr4.CommonTokenStream(lexer);
        const parser = new HandlebarsParser(tokens);
        parser.buildParseTrees = true;
        const tree = parser.document();
        antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

        this.appendHelpers();

        const top = this.popScope();
        return new Function(this._inputVar, top);
    }

    pushScope(){
        this._bodyStack.push(`var ${this._outputVar} = "";\n`);
    }

    popScope() {
        this._bodyStack[this._bodyStack.length - 1] += `return ${this._outputVar};\n`;
        return this._bodyStack.pop();
    }

    appendHelpers() {
        this._usedHelpers.forEach(h => {
            const func = this._helpers.expr[h];
            var str = `var __$${h} = ${func.toString()};\n`;
            this._bodyStack[this._bodyStack.length-1] = str + this._bodyStack[this._bodyStack.length-1];
        })
        this._usedBlocks.forEach(h => {
            const func = this._helpers.block[h];
            var str = `var __$${h} = ${func.toString()};\n`;
            this._bodyStack[this._bodyStack.length-1] = str + this._bodyStack[this._bodyStack.length-1];
        })
    }

    append(expr) {
        this._bodyStack[this._bodyStack.length-1] += `${this._outputVar} += ${expr};\n`;
    }

    exitDataLookup(ctx) {
        const variable = escapeString(ctx.getText())
        ctx.source = `${this._inputVar}.${variable}`;
    }

    exitLiteral(ctx) {
        ctx.source = ctx.getText();
    }

    exitHelperApp(ctx) {
        console.log(ctx);
        
        const helper = ctx.i.text;
        if (helper in this._helpers.expr) {
            this._usedHelpers.add(helper);
            var str = `__$${helper}(${this._inputVar}, `;
            const children = ctx.a;
            for (var i = 0; i < children.length; i++) {
                if (children[i].source.startsWith("__$") || children[i].source.startsWith("_$")) {
                    str += children[i].source;
                } else {
                    var exp = eval(children[i].source);
                    if (typeof(exp) == 'string') {
                        str += `'${exp}'`
                    } else{
                        str += exp;
                    }
                }
                if (i != children.length - 1) {
                    str += ", ";
                }
            }
            str += `)`;
            ctx.source = str;
        }
    }

    exitExpressionInternal(ctx) {
        if(ctx.d) {
            ctx.source = ctx.d.source
        }
        if(ctx.l) {
            ctx.source = ctx.l.source
        }
        if(ctx.e) {
            ctx.source = ctx.e.source
        }
        if(ctx.h) {
            ctx.source = ctx.h.source
        }
    }

    // literal | helperApp | dataLookup | parenExp;
    exitExpressionElement(ctx) {
        ctx.source = ctx.e.children[0].source;
        this.append(ctx.source);
    }

    exitRawElement(ctx) {
        this.append(`"${escapeString(ctx.getText())}"`);
    }

    enterBlockElement(ctx) {
        this.pushScope();
    }

    exitBlockElement(ctx) {
        var open = ctx.o.id.text;
        var close = ctx.c.id.text;
        if (open !== close) {
            throw new Error(`Block start '${open}' does not match the block end '${close}'.`);
        }
        if (open in this._helpers.block) {
            this._usedBlocks.add(open);
            const a = ctx.o.a;
            const top = this.popScope();
            const f = new Function(this._inputVar, top);
            var str = `__$${ctx.o.id.text}(${this._inputVar},
${f}`
            if (ctx.o.a) {
                for (var i = 0; i < ctx.o.a.length; i++) {
                    str += `,\n${ctx.o.a[i].source}`;
                }
            }
            str += ")"
        }
        this.append(str);
    }
}

function if_func(ctx, body, expr) {
    if (expr) {
        return body(ctx);
    }
    return "";
}

function each_func(ctx, body, list) {
    var results = ""
    list.forEach(e => {
        results += body(e);
    });
    return results;
}

function with_func(ctx, body, field) {
    return body(ctx[field]);
}

exports.HandlebarsCompiler = HandlebarsCompiler;
